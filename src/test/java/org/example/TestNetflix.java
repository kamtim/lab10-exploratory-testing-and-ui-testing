package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestNetflix {
    @Test
    public void testNetflix() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.netflix.com/ru/");

        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("Netflix Россия — Смотрите сериалы и фильмы онлайн", driver.getTitle());

        WebElement cardTitle = driver.findElement(By.xpath("//*[@class='our-story-card-title']"));
        Assert.assertEquals("Фильмы, сериалы и многое другое без ограничений.", cardTitle.getText());

        WebElement cookieButton = driver.findElement(By.xpath("//*[@id='cookie-disclosure']//button"));
        cookieButton.click();

        WebElement authButton = driver.findElement(By.xpath("//*[@class='authLinks redButton']"));
        authButton.click();

        WebElement loginInput = driver.findElement(By.xpath("//*[contains(@class, 'login-input')]//input"));
        loginInput.sendKeys("test@mail.ru");

        WebElement passwordInput = driver.findElement(By.xpath("//*[contains(@class, 'login-input-password')]//input"));
        passwordInput.sendKeys("1234567890");

        WebElement loginButton = driver.findElement(By.xpath("//*[contains(@class, 'login-button')]"));
        loginButton.click();

        WebElement messageError = driver.findElement(By.xpath("//*[contains(@class, 'ui-message-error')]"));
        Assert.assertEquals("Incorrect password. Please try again or you can reset your password.", messageError.getText());

        driver.quit();
    }
}
